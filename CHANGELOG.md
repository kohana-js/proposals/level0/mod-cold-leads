# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [3.10.2](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v3.10.1...v3.10.2) (2023-09-04)


### Bug Fixes

* cannot send copy to email when register using phone ([cf4fa40](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/commit/cf4fa406322f022c25a452c2fc089fc4b37bfe1b))

### [3.10.1](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v3.10.0...v3.10.1) (2023-08-23)


### Bug Fixes

* notification email field still display contact value even have info.email ([0cab0fc](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/commit/0cab0fc325ca94236ccdad91011e198b2e90bfcd))

## [3.10.0](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v3.9.9...v3.10.0) (2023-07-27)


### Features

* add HelperLead ([db5c840](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/commit/db5c8400d82c48b71d486044b2d4a4600b489155))

### [3.9.9](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v3.9.8...v3.9.9) (2023-07-27)

### [3.9.8](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v3.9.7...v3.9.8) (2023-07-24)


### Bug Fixes

* Unexpected identifier ([7997108](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/commit/7997108d06df75c53d4886676af0f9590368a90c))

### [3.9.7](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v3.9.6...v3.9.7) (2023-07-24)

### [3.9.6](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v3.9.5...v3.9.6) (2023-07-24)

### [3.9.5](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v3.9.4...v3.9.5) (2023-07-24)


### Bug Fixes

* verify with area code mismatch ([0e32b26](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/commit/0e32b26b070a0e9cc941131e3dbc85b9be0911ad))

### [3.9.4](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v3.9.3...v3.9.4) (2023-07-14)

### [3.9.3](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v3.9.2...v3.9.3) (2023-07-14)


### Bug Fixes

* register error template ([31c98ba](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/commit/31c98ba0a2ac41a9386ee2343383e505d8c187e8))

### [3.9.2](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v3.9.1...v3.9.2) (2023-07-14)

### [3.9.1](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v3.9.0...v3.9.1) (2023-07-14)


### Bug Fixes

* instance error ([b7b3110](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/commit/b7b3110de2bec35eba9b56ca154ea624643251fb))

## [3.9.0](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v3.8.3...v3.9.0) (2023-07-14)


### Features

* multiple greeting template support ([ad56a63](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/commit/ad56a63a07e26ada6a55bdda3b42ac66a36f11f4))

### [3.8.3](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v3.8.2...v3.8.3) (2023-07-14)


### Bug Fixes

* move recaptcha before orm write ([35ce870](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/commit/35ce870e517fac07d8090190b15ce531722b6118))

### [3.8.2](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v3.8.1...v3.8.2) (2023-07-14)

### [3.8.1](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v3.8.0...v3.8.1) (2023-07-12)


### Bug Fixes

* copy parent id as string ([64a45c3](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/commit/64a45c33e42c000d76c51b8094cf103998b9e801))

## [3.8.0](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v3.7.0...v3.8.0) (2023-07-12)


### Features

* lead member copy values from parent ([2519d29](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/commit/2519d296bf56d6532250e39ea390027d6162cae8))

## [3.7.0](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v3.6.0...v3.7.0) (2023-07-12)


### Features

* .members[] to allow single lead create multiple leads ([639a71d](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/commit/639a71db6ae1012d266a8cdd09e79225c87488bb))

## [3.6.0](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v3.5.0...v3.6.0) (2023-07-12)


### Features

* override sender, cc, bcc from config[type] ([2110f09](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/commit/2110f09fc69cdcccac8c99902fae9cb17a7c070e))

## [3.5.0](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v3.4.1...v3.5.0) (2023-07-11)


### Features

* contact type phone with area code selector ([cce635a](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/commit/cce635a72e29af8dd2f2bf21a4cf983693e51a3e))


### Bug Fixes

* save action ([786ac18](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/commit/786ac181977fdc0745d0f8d8bbc098c7788b421a))

### [3.4.1](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v3.4.0...v3.4.1) (2023-07-10)


### Bug Fixes

* templates ([a17a4d6](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/commit/a17a4d60eeaa042b6d199e86800f31f3fbf20f7a))

## [3.4.0](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v3.3.0...v3.4.0) (2023-07-08)


### Features

* add contact type phone ([aee0f66](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/commit/aee0f668b50b5d492e862e0fd5ae8d24f28fa803))

## [3.3.0](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v3.2.1...v3.3.0) (2023-07-08)


### Features

* allow no html templates ([45480bd](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/commit/45480bdc29e4900cce58d25d27aeb3833a77d2ae))

### [3.2.1](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v3.2.0...v3.2.1) (2023-06-06)

## [3.2.0](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v3.1.4...v3.2.0) (2023-05-18)


### Features

* helper edm sender from config[type] ([4aafae1](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/commit/4aafae1929daf09336600af524839610c7e5702c))

### [3.1.4](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v3.1.3...v3.1.4) (2023-05-18)

### [3.1.3](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v3.1.2...v3.1.3) (2023-05-18)

### [3.1.2](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v3.1.1...v3.1.2) (2023-05-18)


### Bug Fixes

* update model ([7aac281](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/commit/7aac28117b753432d729723264c7a74ea1f8970c))

### [3.1.1](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v3.1.0...v3.1.1) (2023-05-18)


### Bug Fixes

* missing lead ip write ([e9b51ee](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/commit/e9b51eeee546b3d29d77980234d37221661de943))

## [3.1.0](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v3.0.2...v3.1.0) (2023-05-18)


### Features

* log lead IP, hostname and user_agent ([81cefdc](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/commit/81cefdc0417652a8096dd40ababc531f29a89dc6))

### [3.0.2](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v3.0.1...v3.0.2) (2023-05-18)

### [3.0.1](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v3.0.0...v3.0.1) (2023-05-18)

## [3.0.0](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v2.1.1...v3.0.0) (2023-05-18)


### ⚠ BREAKING CHANGES

* generic contact information

### Features

* generic contact information ([00f92e2](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/commit/00f92e2e6669eabb8ef65a69f7c9443d60386b93))

### [2.1.1](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v2.1.0...v2.1.1) (2022-06-21)


### Bug Fixes

* recaptcha secret config location ([f41cb89](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/commit/f41cb8964a0ac2be8ffc43d0fa8a8ce052747f05))

## [2.1.0](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v2.0.6...v2.1.0) (2022-06-21)


### Features

* recaptcha ([a433a15](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/commit/a433a150cd9ed9c942ef3347e85b7e180ed92427))

### [2.0.6](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v2.0.5...v2.0.6) (2022-05-24)

### [2.0.4](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v2.0.3...v2.0.4) (2022-05-24)


### Bug Fixes

* Lead action require lead id, lead info share same id with lead ([1c9318f](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/commit/1c9318f5b07ba50190125dc5787999faea08ec05))

### [2.0.3](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v2.0.2...v2.0.3) (2022-05-24)


### Bug Fixes

* leadinfo require lead_id column ([9e7faa9](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/commit/9e7faa970243899ae58c43297a35afa246ec783d))

### [2.0.2](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v2.0.1...v2.0.2) (2022-05-24)


### Bug Fixes

* write lead action to database ([76e4203](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/commit/76e4203cfa7c3405fb84a748d2bc2bedfd42024c))

### [2.0.1](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v2.0.0...v2.0.1) (2022-05-24)

## [2.0.0](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/compare/v1.0.1...v2.0.0) (2022-05-24)


### ⚠ BREAKING CHANGES

* use form attributes[] to dynamic store Lead Info

### Features

* use form attributes[] to dynamic store Lead Info ([b17397d](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/commit/b17397dccd50b33635e7cc49c7a00670c91835a4))

### 1.0.1 (2021-12-21)


### Bug Fixes

* lead info not parsed to edm ([1ed0046](https://gitlab.com/kohana-js/proposals/level0/mod-cold-leads/commit/1ed0046268cae26768cf00f2b367d1aa702ca9e7))

# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### accept tags: Added, Changed, Removed, Deprecated, Removed, Fixed, Security

